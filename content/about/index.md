+++
title = "About Me"
description = "I am a Data Science Researcher and my study focus on the AI application in healthcare sector."
date = 2024-01-30
template = "non-post-page.html"
[extra]
toc = true
+++

![About](about_ai.png)
> Research experience in data science, NLP, generative AI.

> Personality: ENTJ 

> Hobbits: Hiking, Swimming, Tennis, Piano, Reading, Traveling, Food
